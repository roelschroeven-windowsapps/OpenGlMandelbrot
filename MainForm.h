//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <AppEvnts.hpp>
#pragma message "End of auto-included header files"
//---------------------------------------------------------------------------

#include <boost/smart_ptr.hpp>

namespace ogu { class TCtx; class TProgram; }

struct TGlMandelbrot
{
  void Init();
  void Draw(double x, double y, double ScaleX, double ScaleY);

  boost::scoped_ptr<ogu::TProgram> Program;

  // Locations of uniforms, attributes in shaders
  unsigned aPosition;
  unsigned uCenter;
  unsigned uScale;

  // Vertex buffer (separate arrays for positions and texture coordinates)
  int NrTriangles;
  int NrVertices;
  unsigned Positions;
  void CreateVertexBuffer();
};

class TFormMain : public TForm
{
__published:	// IDE-managed Components
  void __fastcall FormResize(TObject *Sender);
  void __fastcall FormKeyPress(TObject *Sender, wchar_t &Key);
  void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
  void __fastcall FormPaint(TObject *Sender);
  void __fastcall FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
          TPoint &MousePos, bool &Handled);


private:	// User declarations
  boost::shared_ptr<ogu::TCtx> m_GlCtx;
  TGlMandelbrot m_GlMandelbrot;
  double m_X, m_Y, m_Scale;

  void Draw();
  void DevToLog(int X, int Y, double &LogX, double &LogY);

public:		// User declarations
  __fastcall TFormMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
