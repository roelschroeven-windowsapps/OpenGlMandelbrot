//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <boost/foreach.hpp>
#include <TrescoUtilLib.h>

#include "OpenGlUtil.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

namespace ogu // short for OpenGlUtil
{

/////////////////////////////////////////////////////////////////////////////
// Check for OpenGL error
// Throw if there's an error

void CheckError()
{
  std::vector<String> Errors;
  while (true)
    {
    GLenum ErrNr = glGetError();
    if (ErrNr == GL_NO_ERROR)
      break;
    const GLubyte *ErrStr = gluErrorString(ErrNr);
    String ErrMsg;
    if (ErrStr)
      ErrMsg.printf(L"%d: %hs", ErrNr, ErrStr);
    else
      ErrMsg.printf(L"%d: <no message available>", ErrNr);
    Errors.push_back(ErrMsg);
    }
  if (!Errors.empty())
    throw Error(String(L"OpenGL error(s):\n") + StringJoin(Errors, "\n"));
}

/////////////////////////////////////////////////////////////////////////////
// Basic OpenGL context

TCtx::TCtx(HWND hWnd): hWnd(hWnd), hDC(NULL), glRC(NULL)
{
  try
    {
    DoInit();
    }
  catch (Exception &E)
    {
    Cleanup();
    throw;
    }
}

TCtx::~TCtx()
{
  Cleanup();
}

void TCtx::DoInit()
{
  hDC = GetDC(hWnd);

  int a = PFD_TYPE_RGBA;
  // TODO: more configurable?
  const PIXELFORMATDESCRIPTOR pfd =
    {
    sizeof(PIXELFORMATDESCRIPTOR),  // Size Of This Pixel Format Descriptor
    1,                              // Version Number
    PFD_DRAW_TO_WINDOW |            // Format Must Support Window
    PFD_SUPPORT_OPENGL |            // Format Must Support OpenGL
    PFD_DOUBLEBUFFER,               // Must Support Double Buffering
    PFD_TYPE_RGBA,                  // Request An RGBA Format
    32,                             // Select Our Color Depth
    0, 0, 0, 0, 0, 0,               // Color Bits Ignored
    0,                              // No Alpha Buffer
    0,                              // Shift Bit Ignored
    0,                              // No Accumulation Buffer
    0, 0, 0, 0,                     // Accumulation Bits Ignored
    32,                             // 32Bit Z-Buffer (Depth Buffer)
    1,                              // Stencil Buffer
    0,                              // No Auxiliary Buffer
    PFD_MAIN_PLANE,                 // Main Drawing Layer
    0,                              // Reserved
    0, 0, 0                         // Layer Masks Ignored
    };

  int PixelFormat = ChoosePixelFormat(hDC, &pfd);
  if (PixelFormat == 0)
    throw Error(PrintF(L"ChoosePixelFormat() failed: %ls", GetErrorText(GetLastError()).c_str()));
  if (!SetPixelFormat(hDC, PixelFormat, &pfd))
    throw Error(PrintF(L"SetPixelFormat() failed: %ls", GetErrorText(GetLastError()).c_str()));
  glRC = wglCreateContext(hDC);
  if (!glRC)
    throw Error(PrintF(L"wglCreateContext() failed: %ls", GetErrorText(GetLastError()).c_str()));
  MakeCurrent();
  GLenum err = glewInit();
  if (err != GLEW_OK)
    throw Error(PrintF(L"glewInit() failed: %hs", glewGetErrorString(err)));

  // TODO: should those be here?
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glEnable(GL_TEXTURE_2D);
}

void TCtx::Cleanup()
{
  if (glRC && hDC)
    {
    wglMakeCurrent(hDC, NULL);
    wglDeleteContext(glRC);
    glRC = NULL;
    }
  if (hDC)
    {
    ReleaseDC(hWnd, hDC);
    }
}

void TCtx::MakeCurrent()
{
  if (!wglMakeCurrent(hDC, glRC))
    throw Error(PrintF(L"wglMakeCurrent() failed: %ls", GetErrorText(GetLastError()).c_str()));
}

void TCtx::SwapBuffers()
{
  ::SwapBuffers(hDC);
}

/////////////////////////////////////////////////////////////////////////////
// Compile shaders

static String ShaderTypeName(GLenum ShaderType)
{
  switch (ShaderType)
    {
    case GL_COMPUTE_SHADER:         return L"GL_COMPUTE_SHADER";
    case GL_VERTEX_SHADER:          return L"GL_VERTEX_SHADER";
    case GL_TESS_CONTROL_SHADER:    return L"GL_TESS_CONTROL_SHADER";
    case GL_TESS_EVALUATION_SHADER: return L"GL_TESS_EVALUATION_SHADER";
    case GL_GEOMETRY_SHADER:        return L"GL_GEOMETRY_SHADER";
    case GL_FRAGMENT_SHADER:        return L"GL_FRAGMENT_SHADER";
    default:                        return L"<unknown>";
    }
}

static void CheckShader(GLuint hShader, const String &ShaderId)
{
  GLint TestVal;
  glGetShaderiv(hShader, GL_COMPILE_STATUS, &TestVal);
  if (TestVal != GL_FALSE)
    return; // OK!

  GLint Length;
  glGetShaderiv(hShader, GL_INFO_LOG_LENGTH, &Length);
  char *InfoLog = new char[Length];
  glGetShaderInfoLog(hShader, Length, NULL, InfoLog);
  String ErrMsg = PrintF("Failed to compile shader '%ls':\n%hs", ShaderId.c_str(), InfoLog);
  delete [] InfoLog;
  glDeleteShader(hShader);
  throw Error(ErrMsg);
}

int Shader(const char *Code, size_t CodeSize, GLenum ShaderType, const String &Name)
{
  // Compile
  GLuint hShader = glCreateShader(ShaderType);
  glShaderSource(hShader, 1, (const GLchar **)&Code, (const GLint*)&CodeSize);
  glCompileShader(hShader);

  // Check if it worked; if not: create error message, clean up, throw exception
  CheckShader(hShader, Name.IsEmpty() ? ShaderTypeName(ShaderType) : Name);

  return hShader;
}

int ShaderFromResource(const AnsiString &ResourceId, GLenum ShaderType)
{
  TRawResource ShaderResource(ResourceId);
  return Shader(
    ShaderResource.Data(),
    ShaderResource.Size(),
    ShaderType,
    PrintF(L"Resource %hs", ResourceId.c_str()));
}

int ShaderFromFile(const String &Filename, GLenum ShaderType)
{
  TTulMMap MMap(Filename);
  return Shader(
    MMap.Begin(),
    MMap.Size(),
    ShaderType,
    PrintF(L"File %hs", Filename.c_str()));
}

/////////////////////////////////////////////////////////////////////////////
// Class around program object

TProgram::TProgram(const TCollect<GLuint> &Shaders)
{
  Handle = glCreateProgram();
  BOOST_FOREACH (GLuint Shader, Shaders.Values)
    glAttachShader(Handle, Shader);
  glLinkProgram(Handle);
  BOOST_FOREACH (GLuint Shader, Shaders.Values)
    glDeleteShader(Shader);
  Check();
}

TProgram::~TProgram()
{
  glDeleteProgram(Handle);
}

void TProgram::Check()
{
  GLint TestVal;
  glGetProgramiv(Handle, GL_LINK_STATUS, &TestVal);
  if (TestVal != GL_FALSE)
    return; // OK;

  // Didn't work; create error message, clean up, throw exception
  GLint Length;
  glGetProgramiv(Handle, GL_INFO_LOG_LENGTH, &Length);
  char *InfoLog = new char[Length];
  glGetProgramInfoLog(Handle, Length, NULL, InfoLog);
  String ErrMsg = PrintF(L"Failed to link shader program:\n%hs", InfoLog);
  delete [] InfoLog;
  glDeleteProgram(Handle);
  throw Error(ErrMsg);
}

GLint TProgram::GetUniformLocation(const AnsiString &Name)
{
  GLint Loc = glGetUniformLocation(Handle, Name.c_str());
  if (Loc == -1)
    throw Error(PrintF(L"Invalid uniform variable '%hs'", Name.c_str()));
  return Loc;
}

GLint TProgram::GetAttribLocation(const AnsiString &Name)
{
  GLint Loc = glGetAttribLocation(Handle, Name.c_str());
  if (Loc == -1)
    throw Error(PrintF(L"Invalid attrib name '%hs'", Name.c_str()));
  return Loc;
}

void TProgram::Use()
{
  glUseProgram(Handle);
}

}
