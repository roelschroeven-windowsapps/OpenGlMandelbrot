#version 120

attribute vec4 aPosition;
uniform vec2 uCenter;
uniform vec2 uScale;
varying vec2 vXY;

void main(void)
{
  gl_Position = aPosition;
  vXY = aPosition.xy / uScale + uCenter;
}


