#version 400

varying vec2 vXY;

const int max_nr_iters = 500;

vec2 complex_square(vec2 z)
{
  return vec2(
    z.x * z.x - z.y * z.y,
    z.y * z.x + z.x * z.y
    );
}

int Mandelbrot(vec2 c, int max_nr_iters)
{
  vec2 z = vec2(0.0, 0.0);
  for (int i = 0; i < max_nr_iters; ++i)
  {
    if (length(z) > 2.0)
      return i;
    z = complex_square(z) + c;
  }
  return max_nr_iters;
}

vec3 hsv2rgb(in vec3 c)
{
  vec3 rgb = clamp(
    abs(mod(c.x*6.0 + vec3(0.0, 4.0, 2.0), 6.0) - 3.0) - 1.0,
    0.0,
    1.0
    );
  return c.z * mix(vec3(1.0), rgb, c.y);
}

vec3 colormap(int nr_iters)
{
  // It seems best to choose periods that are relatively prime, even though I can't
  // explain exactly why
  const float HUE_CYCLE_PERIOD = 8;
  const float VALUE_CYCLE_PERIOD = 53;

  // Points in the set: always black
  if (nr_iters == max_nr_iters)
    return vec3(0.0, 0.0, 0.0);

  // Hue: based on the logarithm of nr_iter, because the color bands get too narrow
  // in highly detailed areas when we use nr_iter itself. We also cycle round and
  // round the color circle in principle (just in case), but in reality with common
  // values max_nr_iter we don't reach the end of the first cycle (with cycle period 8,
  // we'd need e^8 = 2981 iterations to reach the end).
  float hue = mod(log(nr_iters), HUE_CYCLE_PERIOD) / HUE_CYCLE_PERIOD;
  // Saturation: fixed 1.0
  float saturation = 1.0;
  // Value: cycle up and down between 0.2 and 1.0
  float value = mix(0.2, 1.0, abs(mix(-1.0, 1.0, mod(nr_iters + 0.5*VALUE_CYCLE_PERIOD, VALUE_CYCLE_PERIOD) / VALUE_CYCLE_PERIOD)));

  return hsv2rgb(vec3(hue, saturation, value));
}

void main(void)
{
  int nr_iters = Mandelbrot(vXY, max_nr_iters);
  vec3 color = colormap(nr_iters);
  gl_FragColor = vec4(color, 1.0);
}
