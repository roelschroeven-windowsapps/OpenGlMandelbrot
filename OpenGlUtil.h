//---------------------------------------------------------------------------

#ifndef OpenGlUtilH
#define OpenGlUtilH
//---------------------------------------------------------------------------

#include "GL/glew.h"

namespace ogu // short for OpenGlUtil
{

struct Error: public Exception { Error(const String &Msg): Exception(Msg) { } };

void CheckError();

class TCtx
{
public:
  TCtx(HWND hWnd);
  ~TCtx();

  HWND hWnd;
  HDC hDC;
  HGLRC glRC;

  void MakeCurrent();
  void SwapBuffers();

private:
  void DoInit();
  void Cleanup();
};

int Shader(const char *Code, size_t CodeSize, GLenum ShaderType, const String &Name=L"");
int ShaderFromResource(const AnsiString &ResourceId, GLenum ShaderType);
int ShaderFromFile(const String &Filename, GLenum ShaderType);

template<class T> struct TCollect
{
  TCollect& Add(const T &Val) { Values.push_back(Val); return *this; }
  TCollect& operator<<(const T &Val) { Values.push_back(Val); return *this; }

  std::vector<T> Values;
};

class TProgram
{
public:
  TProgram(const TCollect<GLuint> &Shaders);
  ~TProgram();

  void Use();

  GLint GetUniformLocation(const AnsiString &Name);
  GLint GetAttribLocation(const AnsiString &Name);

  GLuint Handle;

private:
  void Check();
};

}

#endif
