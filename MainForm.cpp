//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#pragma link "TrescoUtilLib.lib"

#include <math.h>
#include <boost/foreach.hpp>
#include <TrescoUtilLib.h>
#include "GL/glew.h"

#include "MainForm.h"
#include "OpenGlUtil.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMain *FormMain;
//---------------------------------------------------------------------------

void TGlMandelbrot::Init()
{
  NrTriangles = 2;

  ogu::TCollect<GLuint> Shaders;
  Shaders
    << ogu::ShaderFromResource("Resource_MandelbrotVert", GL_VERTEX_SHADER)
    << ogu::ShaderFromResource("Resource_MandelbrotFrag", GL_FRAGMENT_SHADER);
  Program.reset(new ogu::TProgram(Shaders));

  aPosition = Program->GetAttribLocation("aPosition");
  uCenter = Program->GetUniformLocation("uCenter");
  uScale = Program->GetUniformLocation("uScale");

  glEnableVertexAttribArray(aPosition);

  CreateVertexBuffer();
}

void TGlMandelbrot::CreateVertexBuffer()
{
  NrVertices = 4;
  const float PositionData[] =
    {
      -1.0, -1.0,
       1.0, -1.0,
       1.0,  1.0,
      -1.0,  1.0,
    };
  glGenBuffers(1, &Positions);
  glBindBuffer(GL_ARRAY_BUFFER, Positions);
  glBufferData(GL_ARRAY_BUFFER, sizeof(PositionData), (GLvoid*)PositionData, GL_STATIC_DRAW);
}

void TGlMandelbrot::Draw(double x, double y, double ScaleX, double ScaleY)
{
  Program->Use();
  glUniform2f(uCenter, x, y);
  glUniform2f(uScale, ScaleX, ScaleY);

  glBindBuffer(GL_ARRAY_BUFFER, Positions);
  glVertexAttribPointer(aPosition, 2, GL_FLOAT, false, 0, 0);

  glDrawArrays(GL_TRIANGLE_FAN, 0, NrVertices);
}
//---------------------------------------------------------------------------


__fastcall TFormMain::TFormMain(TComponent* Owner)
  : TForm(Owner)
  , m_X(0.0)
  , m_Y(0.0)
  , m_Scale(0.5)
{
  m_GlCtx.reset(new ogu::TCtx(Handle));
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  m_GlMandelbrot.Init();

  WindowState = wsMaximized;
}
//---------------------------------------------------------------------------

void TFormMain::Draw()
{
  Caption = PrintF(L"Simplistic Mandelbrot set | %f, %f  x%f", m_X, m_Y, m_Scale);
  glViewport(0, 0, ClientWidth, ClientHeight);
  glClear(GL_COLOR_BUFFER_BIT);
  m_GlMandelbrot.Draw(m_X, m_Y, m_Scale, m_Scale * ClientWidth / ClientHeight);
  m_GlCtx->SwapBuffers();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormPaint(TObject *Sender)
{
  Draw();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormResize(TObject *Sender)
{
  Draw();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormKeyPress(TObject *Sender, wchar_t &Key)
{
  switch (Key)
    {
    case L'+':
      m_Scale *= 1.1;
      Draw();
      break;
    case L'-':
      m_Scale /= 1.1;
      Draw();
      break;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta, TPoint &MousePos, bool &Handled)
{
  if (WheelDelta > 0)
    m_Scale *= 1.1;
  else
    m_Scale /= 1.1;
  Draw();
}
//---------------------------------------------------------------------------

void TFormMain::DevToLog(int X, int Y, double &LogX, double &LogY)
{
  double ScaleX = m_Scale;
  double ScaleY = m_Scale * ClientWidth / ClientHeight;
  LogX = 2.0 * X / ClientWidth - 1.0;
  LogY = - (2.0 * Y / ClientHeight - 1.0);
  LogX = (LogX / ScaleX + m_X);
  LogY = (LogY / ScaleY + m_Y);
}

void __fastcall TFormMain::FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
  double LogX, LogY;
  DevToLog(X, Y, LogX, LogY);
  m_X = LogX;
  m_Y = LogY;
  Draw();
}
//---------------------------------------------------------------------------

